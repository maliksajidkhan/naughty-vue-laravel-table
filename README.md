# Naughty Vue Laravel Table
This is a simple package of vuejs which enables you to handle the default pagination of laravel in vuejs. This is intial version of this package and may contain bugs and i'm actively working on it and will upload the newest version soon.

## Configuration
This package is designed for axios. Paste below snippets in your app.js. Remove the authentication line if you don't work with token based authentication.

```
// axios header
axios.defaults.headers.common = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Authorization': 'Bearer ' + sessionStorage.getItem('token')
};
```

## Import the plugin in app.js

```
//NaughtyVueLaravelTable
import Vue from 'vue';
import NaughtyVueLaravelTable from "naughty-vue-laravel-table";
Vue.use(NaughtyVueLaravelTable);
```

## Usage
Paste the below code in template of your component

```
<naughty-vue-laravel-table
    :tableConfiguration="tableConfiguration"
    :pagination="pagination"
    :perPage="perPage"
    :columns="columns"
    :apiURL="apiURL"
  ></naughty-vue-laravel-table>
```

## Paste the below code inside the return object of your data function and change according to your need.

```
 apiURL: "api/campus",
      tableConfiguration: {
        defaultLength: 10,
        searchGlobal: null,
        searchColumnName: "name",
        searchColumnText: null,
        column: 0,
        direction: "desc",
        specificPage: null,
        sortColumn: "email",
        sortOrder: "asc"
      },
      pagination: {
        lastPage: null,
        currentPage: null,
        total: null,
        lastPageUrl: null,
        nextPageUrl: null,
        prevPageUrl: null,
        from: null,
        to: null
      },
      perPage: ["10", "20", "30"],
      columns: [
        { width: "20%", label: "Name", name: "name" },
        { width: "20%", label: "Email", name: "email" },
        { width: "20%", label: "Phone", name: "phone" },
        { width: "20%", label: "City", name: "city" },
        { width: "20%", label: "Action", name: "action" }
      ]
      
```

## Laravel Function

```
 public function index(Request $request)
 {
  $columns = ['name', 'phone', 'email', 'city'];
  $defaultLength   = $request->input('defaultLength');
  $column   = $request->input('column');
  $direction      = $request->input('direction');
  $searchColumnName = $request->input('searchColumnName');
  $searchColumnText = $request->input('searchColumnText');
  $searchGlobal     = $request->input('searchGlobal');

  $specificPage = $request->input('specificPage');

  if ($specificPage) {
   Paginator::currentPageResolver(function () use ($specificPage) {
    return $specificPage;
   });
  }
  $query = Campus::select('id', 'name', 'phone', 'email', 'city')->orderBy($columns[$column], $direction);

  if ($searchGlobal) {
   $query->where(function ($query) use ($searchGlobal) {
    $query->where('name', 'like', '%' . $searchGlobal . '%')
     ->orWhere('email', 'like', '%' . $searchGlobal . '%');
   });
  }

  if ($searchColumnName) {
   $query->where(function ($query) use ($searchColumnName, $searchColumnText) {
    $query->where($searchColumnName, 'like', '%' . $searchColumnText . '%');
   });
  }

  $daata = $query->paginate($defaultLength);
  return ['data' => $daata];
 }

```